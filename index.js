const {auth} = require('google-auth-library');
const request = require('request');
const escapeHtml = require('escape-html');

/**
 * HTTP Cloud Function.
 *
 * @param {Object} req Cloud Function request context.
 *                     More info: https://expressjs.com/en/api.html#req
 * @param {Object} res Cloud Function response context.
 *                     More info: https://expressjs.com/en/api.html#res
 */
exports.deploy = (req, res) => {
  	const version = req.query.version;
	const environment = req.query.environment;
  
  	let env = process.env.ENV;
  	let environmentProject;	
  	if(env === "test"){
      environmentProject = "hello-gcp-test-258313";
    }else if (env === "prod") {
       environmentProject = "hello-gcp-prod";
    } 
	
		auth.getClient().then(function(authClient) {
			authClient.getAccessToken().then(function(accessToken) {
				const buildRequest = {
					uri: 'https://cloudbuild.googleapis.com/v1/projects/hello-gcp-257615/builds',
					method: 'POST',
					auth: {
						'bearer': accessToken.token
					},
					json: {
						'steps': [{
									"name": "gcr.io/cloud-builders/gcloud",
								     "args": [
								     "beta",
								     "run",
								     "deploy",
								     env,
								     "--image",
								     "gcr.io/hello-gcp-257615/hello-gcp",
								     "--region",
								     [
								       "us-central1"
								     ],
								     "--platform",
								     "managed",
								     "--allow-unauthenticated",
								     "--project",
								     environmentProject
								   ]
							
						}]
					}
				};
				request(buildRequest, function(error, response, body) {
					if(!error) {
						res.send(body);
					} else {
						res.send(error);
					}
				});
			}, function(error) {
				res.send(error);
			});
		}, function(error) {
			res.send(error);
		});
	
};

